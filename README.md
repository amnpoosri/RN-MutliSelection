# React-Native Multi-Selection Component
A multi (or single) select component. The selected item will move to the top and ordey by alphabetical order. Also, the component allow for search item.

IOS            |  ANDROID
:-------------------------:|:-------------------------:
<img src="preview/ios_multi_selection.gif" alt="IOS" width="250">  |  <img src="preview/android_multi_selection.gif" alt="IOS" width="250">
        

## Usage

Required props:  
`items` | array  
`uniqueKey` | string  
`onSelectedItemsChange` | function  

```javascript
import React, {Component} from 'react';
import { StyleSheet, View, StatusBar} from 'react-native';
import MultiSelection from './components/MultiSelection'
const items = [
      {
          "id": 11,
          "name": "Airsoft"
      },
      {
          "id": 21,
          "name": "Basketball"
      },
      {
          "id": 31,
          "name": "biking"
      },
      {
          "id": 41,
          "name": "Bootcamp"
      },
      {
          "id": 51,
          "name": "Boxing"
      },
      {
          "id": 61,
          "name": "Energetic meditation"
      },
      {
          "id": 71,
          "name": "Energy meditation"
      },
      {
          "id": 81,
          "name": "Enery Meditation"
      },
      {
          "id": 91,
          "name": "Football"
      },
      {
          "id": 101,
          "name": "Kickboxing"
      },
      {
          "id": 111,
          "name": "Running"
      },
      {
          "id": 121,
          "name": "Skateboarding"
      },
      {
          "id": 131,
          "name": "Street soccer"
      },
      {
          "id": 141,
          "name": "Street workout"
      },
      {
          "id": 151,
          "name": "swimming"
      },
      {
          "id": 161,
          "name": "Volleyball"
      },
      {
          "id": 171,
          "name": "Yoga"
      }]
  

export default class App extends Component {

    static propTypes = {

      };

    constructor(props: PropsType) {
        super(props);
        this.state = {
            selectedItems: []
          }  
      }

    onSelectedItemsChange = selectedItems => {
        this.setState({ selectedItems });
      };

  render() {
    return (
      <View style={styles.container}>
        <MultiSelection 
          items={items}
          uniqueKey="id"
          displayKey="name"
          selectedItems={this.state.selectedItems}
          onSelectedItemsChange={this.onSelectedItemsChange}/>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
});
```

### Props
| Prop        					| Default       | type 			| Desc  |
| ------------- 				|-------------	| -----			|-----	|
|uniqueKey              | 'id'          | string    |the unique key for your items |
|displayKey           | 'name'         | string    |the key for the display name / title of the item |
|selectedItems					| []						| array 		|the selected items |
|onSelectedItemsChange	| 	| function	|function that runs when an item is toggled|