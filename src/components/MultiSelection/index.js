import React, {Component} from 'react';
import {Text, TouchableOpacity, View, TextInput, FlatList, StyleSheet} from 'react-native';
import PropTypes from 'prop-types';

import {differenceBy,differenceWith, reject, find, get, filter, indexOf, orderBy, assign} from 'lodash';

import {moderateScale,} from '../../utils/scaling';
import Search from '../Search'
import RowItem from './RowItem'

class MultiSelection extends Component {

  static propTypes = {
    selectedItems: PropTypes.array,
    items: PropTypes.array.isRequired,
    uniqueKey: PropTypes.string,
    canAddItems: PropTypes.bool,
    onAddItem: PropTypes.func,
    onChangeInput: PropTypes.func,
    displayKey: PropTypes.string,
    onSelectedItemsChange: PropTypes.func.isRequired,
  };

  static defaultProps = {
    selectedItems: [],
    items: [],
    uniqueKey: 'id',
    onChangeInput: () => {},
    displayKey: 'name',
    canAddItems: false,
    onAddItem: () => {},
    onSelectedItemsChange: () => {},
  };

  constructor(props: PropsType) {
    super(props);
    this.state = {
      searchTerm: '',
    }
  }

  onSearchTextChange = (text) => {
    this.setState({ searchTerm: text });
  }

  isItemSelected = item => {
    const { uniqueKey, selectedItems } = this.props;
    var obj = selectedItems.find(function (selectedItem) {
      return selectedItem[uniqueKey] === item[uniqueKey]; 
    });
    return obj? true : false
  };

  onRowItemClicked = item => {
    const {
      uniqueKey,
      selectedItems,
      onSelectedItemsChange
    } = this.props;
    const status = this.isItemSelected(item);
    let newItems = [];
    if (status) {
        newItems = reject(
          selectedItems,
          singleItem => item[uniqueKey] === singleItem[uniqueKey]
        );
      } else {
        newItems = [...selectedItems, item];
      }
      console.log()
    onSelectedItemsChange(newItems);
  };

  filterItems = searchTerm => {
    const { items, displayKey } = this.props;
    const filteredItems = [];
    items.forEach(item => {
      const parts = searchTerm.trim().split(/[ \-:]+/);
      const regex = new RegExp(`(${parts.join('|')})`, 'ig');
      if (regex.test(get(item, displayKey))) {
        filteredItems.push(item);
      }
    });
    return filteredItems;
  };

  _renderFlatList = (items) => {
    const {
      canAddItems,
      uniqueKey,
      selectedItems
    } = this.props;
    const { searchTerm } = this.state;
    let component = null;
    // If searchTerm matches an item in the list, we should not add a new
    // element to the list.
    let searchTermMatch;
    let itemList;
    let addItemRow;
    const renderItems = searchTerm ? this.getOrderItems(this.filterItems(searchTerm)) : items;
    if (renderItems.length) {
      itemList = (
        <FlatList
          data={renderItems}
          extraData={selectedItems}
          keyExtractor={item => item[uniqueKey]}
          renderItem={rowData => 
            <RowItem 
              item = {rowData.item}
              onPress = {this.onRowItemClicked}/>
          }/>
      );
      searchTermMatch = renderItems.filter(item => item.name === searchTerm)
        .length;
    } else if (!canAddItems) {
      itemList = (
        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
          <Text
            style={{
                flex: 1,
                marginTop: 20,
                textAlign: 'center',
                color: '#ff0000'}}>
            Cannot find the items
          </Text>
        </View>
      );
    }

    if (canAddItems && !searchTermMatch && searchTerm.length) {
      addItemRow = this._getRowNew({ name: searchTerm });
    }
    component = (
      <View>
        {itemList}
        {addItemRow}
      </View>
    );
    return component;
  };
  
  getOrderItems = (items) => {
    return orderBy(items, ['isSelected', 'name'], ['desc', 'asc'])
  }

  getSortedItemChecklist = (items) => {
    var self = this;

    items.forEach(function(item){item.isSelected =  self.isItemSelected(item)});
    return this.getOrderItems(this.props.items)
  }


  render() {

    return (
      <View style={styles.multiSelectionContainer}>
        <Search 
          onChangeText={this.onSearchTextChange}/>
        <View>{this._renderFlatList(this.getSortedItemChecklist(this.props.items))}</View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
    multiSelectionContainer: {
    flex: 1,
    flexDirection: 'column'
  },

});

export default MultiSelection;